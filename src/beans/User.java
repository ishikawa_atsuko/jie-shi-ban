package beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private static Map<Integer, String> branches = new HashMap<>();
    private static Map<Integer, String> departments = new HashMap<>();

    static {
    	branches.put(1, "本社");
    	branches.put(2, "福岡支店");
    	branches.put(100, "大阪支店");
    	branches.put(102, "名古屋支店");
    	branches.put(105, "北海道支店");
    	branches.put(112, "仙台支店");

    	departments.put(1, "総務");
    	departments.put(2, "支店長");
    	departments.put(103, "係長");
    	departments.put(100, "平社員");
    	departments.put(102, "派遣社員");
    	departments.put(106, "課長");
    	departments.put(107, "バイト");
    }

    public static Map<Integer, String> getBranches() {
    	return branches;
    }

    public static Map<Integer, String> getDepartments() {
    	return departments;
    }

    private int id;
    private String userid;
    private String password;
    private String passwordconfirm;
    private String name;
    private int branchnumber;
    private int departmentnumber;

    public String getBranchName() {
    	return branches.get(branchnumber);
    }
    public String getDepartmentName() {
    	return departments.get(departmentnumber);
    }
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userid;
	}
	public void setUserId(String userid) {
		this.userid = userid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirm() {
		return passwordconfirm;
	}
	public void setPasswordConfirm(String passwordconfirm) {
		this.passwordconfirm = passwordconfirm;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranchNumber() {
		return branchnumber;
	}
	public void setBranchNumber(int branchnumber) {
		this.branchnumber = branchnumber;
	}
	public int getDepartmentNumber() {
		return departmentnumber;
	}
	public void setDepartmentNumber(int departmentnumber) {
		this.departmentnumber = departmentnumber;
	}
}