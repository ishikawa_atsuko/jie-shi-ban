package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	Integer id = Integer.parseInt(request.getParameter("id"));

        User editUser = new UserService().getUser(id);
        request.setAttribute("editUser", editUser);
    	request.setAttribute("branches", User.getBranches());
    	request.setAttribute("departments", User.getDepartments());

        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<>();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {
			new UserService().update(editUser);
			response.sendRedirect("./");
			return;
		}
		request.setAttribute("errorMessages", messages);
		request.setAttribute("editUser", editUser);
    	request.setAttribute("branches", User.getBranches());
    	request.setAttribute("departments", User.getDepartments());

		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setUserId(request.getParameter("userid"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setPasswordConfirm(request.getParameter("passwordconfirm"));
		editUser.setBranchNumber(Integer.parseInt(request.getParameter("branchnumber")));
		editUser.setDepartmentNumber(Integer.parseInt(request.getParameter("departmentnumber")));
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("userid");
		String password = request.getParameter("password");
		String passwordconfirm = request.getParameter("passwordconfirm");

		if (StringUtils.isEmpty(account) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}
		if (!password.equals(passwordconfirm)) {
			messages.add("パスワードを入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}