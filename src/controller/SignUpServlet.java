package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	request.setAttribute("branches", User.getBranches());
    	request.setAttribute("departments", User.getDepartments());

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<>();
        User user = new User();
        user.setName(request.getParameter("name"));
        user.setUserId(request.getParameter("userid"));
        user.setPassword(request.getParameter("password"));
        user.setPasswordConfirm(request.getParameter("passwordconfirm"));
        user.setBranchNumber(Integer.parseInt(request.getParameter("branchnumber")));
        user.setDepartmentNumber(Integer.parseInt(request.getParameter("departmentnumber")));

        if (isValid(request, messages) == true) {
            new UserService().register(user);

            response.sendRedirect("./");
            return;
        }

        request.setAttribute("user", user);
        request.setAttribute("errorMessages", messages);
    	request.setAttribute("branches", User.getBranches());
    	request.setAttribute("departments", User.getDepartments());
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String userid = request.getParameter("userid");
        String password = request.getParameter("password");
		String passwordconfirm = request.getParameter("passwordconfirm");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(name)) {
            messages.add("ユーザー名を入力してください");
        }
        if (name.length() >10) {
        	messages.add("ユーザー名は10文字以内で入力してください");
        }

        if (StringUtils.isEmpty(userid)) {
        	messages.add("ログインIDを入力してください");
        }
        if (!userid.matches("\\w{6,20}")) {
        	messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
        }
        if (StringUtils.isEmpty(password)) {
        	messages.add("パスワードを入力してください");
        }
        if (!password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+${6,20}")) {
        	messages.add("パスワードは記号を含む全ての半角英数字6文字以上20文字以下で入力してください");
        }
        if (!password.equals(passwordconfirm)) {
        	messages.add("パスワードが違います");
        }

        if (name.length() > 11) {
        	messages.add("名前のフォーマットが不正です");
        }

        return messages.size() == 0;
    }
}