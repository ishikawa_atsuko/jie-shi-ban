package dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("user_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_number");
            sql.append(", department_number");
            sql.append(") VALUES (");
            sql.append("?"); // account
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_number
            sql.append(", ?"); // department_number
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getUserId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchNumber());
            ps.setInt(5, user.getDepartmentNumber());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String accountOrEmail,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (account = ? OR email = ?) AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, accountOrEmail);
            ps.setString(2, accountOrEmail);
            ps.setString(3, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public List<User> getUsers(Connection conn) {
    	PreparedStatement ps = null;

    	try {
    		ps = conn.prepareStatement("SELECT * FROM users");
    		ResultSet rs = ps.executeQuery();
    		return toUserList(rs);
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
			close(ps);
		}
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {
    	List<User> ret = new ArrayList<>();

    	try {
    		while (rs.next()) {
    			User user = new User();
    			user.setId(rs.getInt("id"));
    			user.setUserId(rs.getString("user_id"));
    			user.setName(rs.getString("name"));
    			user.setPassword(rs.getString("password"));
    			user.setBranchNumber(rs.getInt("branch_number"));
    			user.setDepartmentNumber(rs.getInt("department_number"));

    			ret.add(user);
    		}

    		return ret;
    	} finally {
    		close(rs);
    	}
    }

    public void deleteUser(Connection conn, int id) {
    	PreparedStatement ps = null;
    	try {
    		String sql = "DELETE FROM users WHERE id = ?";
    		ps = conn.prepareStatement(sql);
    		ps.setInt(1, id);

    		ps.executeUpdate();
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public User getUser(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  user_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_number = ?");
			sql.append(", department_number = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getUserId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchNumber());
			ps.setInt(5, user.getDepartmentNumber());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}