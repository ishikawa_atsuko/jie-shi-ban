package service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import chapter6.utils.CipherUtil;
import dao.UserDao;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<User> getUsers() {
    	Connection conn = null;
    	try {
    		conn = getConnection();

    		UserDao dao = new UserDao();
    		List<User> ret = dao.getUsers(conn);

    		commit(conn);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(conn);
    		throw e;
    	} catch (Error e) {
    		rollback(conn);
    		throw e;
    	} finally {
    		close(conn);
    	}
    }

    public User getUser(int userId) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserDao userDao = new UserDao();
    		User user = userDao.getUser(connection, userId);

    		commit(connection);

    		return user;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void delete(Integer id) {
    	Connection conn = null;
    	try {
    		conn = getConnection();

    		UserDao dao = new UserDao();
    		dao.deleteUser(conn, id);
    		commit(conn);

    	} catch (RuntimeException e) {
    		rollback(conn);
    		throw e;
    	} catch (Error e) {
    		rollback(conn);
    		throw e;
    	} finally {
    		close(conn);
    	}
    }

    public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}