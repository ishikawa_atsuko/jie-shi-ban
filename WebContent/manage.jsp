<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
<body>
			<div>
				<c:if test="${ empty loginUser }">
					<a href="signup">登録する</a><br/><br/>
					<a href="edit">ユーザー編集画面</a>
				</c:if>
			</div>

			<table>
				<tr>
					<th>名前</th>
					<th>ログインID</th>
					<th>支店</th>
					<th>部署・役職</th>
					<th></th>
				</tr>
				<c:forEach items="${users}" var="user">
					<tr>
						<td><a href="setting?id=${user.id}">${user.name}</a></td>
						<td>${user.userId}</td>
						<td>${user.branchName}</td>
						<td>${user.departmentName}</td>
						<td>
							<form action="delete" method="POST">
								<input type="hidden" name="id" value="${user.id}">
								<input type="submit" value="削除" />
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>
</body>
</html>