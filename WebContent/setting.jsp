<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.account}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="setting" method="post"><br />
                <input name="id" value="${editUser.id}" id="id" type="hidden"/>
                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name"/>（名前はあなたの公開プロフィールに表示されます）<br />

                <label for="userid">ログインID</label>
                <input name="userid" value="${editUser.userId}" id="userid" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />
                <label for="passwordconfirm">確認用パスワード (上記のパスワードをコピーせずに記入ください。)</label>
                <input name="passwordconfirm" type="password" id="passwordconfirm" />

                <label for="branchnumber">支店</label>
                <select name="branchnumber" id="branchnumber">
                	<c:forEach items="${branches}" var="branchnumber">
                		<option value="${branchnumber.key}" <c:if test='${editUser.branchNumber==branchnumber.key}'>selected</c:if>>${branchnumber.value}</option>
                	</c:forEach>
                </select> <br />

                 <label for="departmentnumber">役職</label>
                    <select name="departmentnumber" id="departmentnumber">
                		<c:forEach items="${departments}" var="departmentnumber">
                			<option value="${departmentnumber.key}" <c:if test='${editUser.departmentNumber==departmentnumber.key}'>selected</c:if>>${departmentnumber.value}</option>
                		</c:forEach>
                	</select>


                <input type="submit" value="登録" /> <br />
                <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Your Name</div>
        </div>
    </body>
</html>