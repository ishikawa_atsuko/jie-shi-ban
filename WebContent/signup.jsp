<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${not empty errorMessages}">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<br /> ユーザー新規登録<br />
		<form action="signup" method="post">
			<br />
		    <label for="name">ユーザー名(10文字以内)</label>
		    <br />
		    <input type="text" name="name" id="name" size="20" />
		    <br />
		    <label for="branchnumber">支店コード</label>
		    <br />
		    <select name="branchnumber">
                	<c:forEach items="${branches}" var="branch">
                		<option value="${branch.key}" <c:if test='${user.branchNumber==branch.key}'>selected</c:if>>${branch.value}</option>
                	</c:forEach>
                </select>
		    <br />
		    <label for="departmentnumber">部署（役職）コード</label>
		    <br />
		   <select name="departmentnumber">
                		<c:forEach items="${departments}" var="department">
                			<option value="${department.key}" <c:if test='${user.departmentNumber==department.key}'>selected</c:if>>${department.value}</option>
                		</c:forEach>
                	</select>
		    <br />
			<label for="userid">ログインID（半角英数字6文字以上20文字以下）</label>
			<br />
			<input type="text" name="userid" size="25" id="userid" />
			<br />
			<label for="password">パスワード（記号を含む全ての半角数字6文字以上20文字以下）</label>
			<br />
			<input type="password" name="password" id="password" size="25" />
			<br />
			<label for="passwordconfirm">パスワード（確認用）</label>
			<br />
			<input type="password" name="passwordconfirm" id="passwordconfirm" size="25" />
			<br />
			<br />
			<input type="submit" value="登録" />
			<br />
			<a href="./">戻る</a>
		</form>
	</div>
</body>
</html>